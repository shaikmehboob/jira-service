package com.rdops.microservices.jiraservice;

import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@RestController
public class JiraController {
	
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping("/hello/{name}")
	public String hello(@PathVariable String name) {
		return "Hello, "+name;
	}
	
	@PostMapping("/getCurrentUser")
	@ResponseBody
	public String getCurrentUser(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {
		
        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.get("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/myself")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/createJiraProject")
	@ResponseBody
	public String createJiraProject(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

		JSONObject json = new JSONObject();
		json.put("name", "RestApi");
		json.put("key", "REST");
		json.put("projectTypeKey", "software");
		json.put("lead", "admin");
		System.out.println(json);

		byte[] credential = Base64.encodeBase64((jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

		HttpResponse<JsonNode> response = Unirest.post("https://jira-service.atlassian.net/rest/api/2/project")
		  .header("Authorization","Basic " + credentialEnc)
		  .header("Accept", "application/json")
		  .header("Content-Type", "application/json")
		  .body(json)
		  .asJson();

		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/getJiraProjects")
	@ResponseBody
	public String getJiraProjects(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.get("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/project")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/getJiraProjectById")
	@ResponseBody
	public String getJiraProjectById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.get("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/project/RDOP")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/updateJiraProjectById")
	@ResponseBody
	public String updateJiraProjectById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

		JSONObject json = new JSONObject();
		json.put("key", "RDP");
		json.put("name", "Reliable DevOps");
		System.out.println(json);

		byte[] credential = Base64.encodeBase64((jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

		HttpResponse<JsonNode> response = Unirest.put("https://jira-service.atlassian.net/rest/api/2/project/10000")
		  .header("Authorization","Basic " + credentialEnc)
		  .header("Accept", "application/json")
		  .header("Content-Type", "application/json")
		  .body(json)
		  .asJson();

		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@DeleteMapping("/deleteJiraProjectById")
	@ResponseBody
	public String deleteJiraProjectById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.delete("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/project/RSAP")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/createJiraIssue")
	@ResponseBody
	public String createJiraIssue(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

		JSONObject json = new JSONObject();
		JSONObject project = new JSONObject();
		JSONObject key = new JSONObject();
		JSONObject id = new JSONObject();
		key.put("key", "RDOP");
		id.put("id", 10002);
		project.put("project", key);
		project.put("summary", "test summary");
		project.put("issuetype", id);
		json.put("fields", project);
		System.out.println(json);

		byte[] credential = Base64.encodeBase64((jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

		HttpResponse<JsonNode> response = Unirest.post("https://jira-service.atlassian.net/rest/api/2/issue")
		  .header("Authorization","Basic " + credentialEnc)
		  .header("Accept", "application/json")
		  .header("Content-Type", "application/json")
		  .body(json)
		  .asJson();

		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}

	@PostMapping("/getJiraIssues")
	@ResponseBody
	public String getJiraIssues(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {
		
        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.get("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/search?filter=allissues")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/getJiraIssueById")
	@ResponseBody
	public String getJiraIssueById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		HttpResponse<JsonNode> response = Unirest.get("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/issue/RDOP-1")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/updateJiraIssueById")
	@ResponseBody
	public String updateJiraIssueById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

		JSONObject json = new JSONObject();
		JSONObject update = new JSONObject();
		JSONObject summ = new JSONObject();
		JSONObject desc = new JSONObject();
		JSONArray arr1 = new JSONArray();
		JSONArray arr2 = new JSONArray();
		summ.put("set", "Hello Again");
		desc.put("set", "Hello Again");
		arr1.put(summ);
		arr2.put(desc);
		update.put("summary", arr1);
		update.put("description", arr2);
		json.put("update", update);
		System.out.println(json);

		byte[] credential = Base64.encodeBase64((jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

		HttpResponse<JsonNode> response = Unirest.put("https://jira-service.atlassian.net/rest/api/2/issue/RDP-1")
		  .header("Authorization","Basic " + credentialEnc)
		  .header("Accept", "application/json")
		  .header("Content-Type", "application/json")
		  .body(json)
		  .asJson();

		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@DeleteMapping("/deleteJiraIssueById")
	@ResponseBody
	public String deleteJiraIssueById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.delete("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/issue/RSAP-1")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/addCommentToJiraIssue")
	@ResponseBody
	public String addCommentToJiraIssue(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

		JSONObject json = new JSONObject();
		json.put("body", "New comment");
		System.out.println(json);

		byte[] credential = Base64.encodeBase64((jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

		HttpResponse<JsonNode> response = Unirest.post("https://jira-service.atlassian.net/rest/api/2/issue/RDOP-1/comment")
		  .header("Authorization","Basic " + credentialEnc)
		  .header("Accept", "application/json")
		  .header("Content-Type", "application/json")
		  .body(json)
		  .asJson();

		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/getJiraIssueComments")
	@ResponseBody
	public String getJiraIssueComments(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.get("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/issue/RSAP-1/comment")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@DeleteMapping("/deleteJiraIssueCommentById")
	@ResponseBody
	public String deleteJiraIssueCommentById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.delete("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/issue/RSAP-1/comment/10004")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/createJiraComponent")
	@ResponseBody
	public String createJiraComponent(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

		JSONObject json = new JSONObject();
		json.put("name", "New component");
		json.put("project", "RDOP");
		json.put("assigneeType", "PROJECT_LEAD");
		json.put("isAssigneeTypeValid", false);
		json.put("projectId", 10000);
		json.put("description", "Description of the component");
		System.out.println(json);

		byte[] credential = Base64.encodeBase64((jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

		HttpResponse<JsonNode> response = Unirest.post("https://jira-service.atlassian.net/rest/api/2/component")
		  .header("Authorization","Basic " + credentialEnc)
		  .header("Accept", "application/json")
		  .header("Content-Type", "application/json")
		  .body(json)
		  .asJson();

		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/getJiraComponentById")
	@ResponseBody
	public String getJiraComponentById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.get("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/component/10000")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@PostMapping("/updateJiraComponentById")
	@ResponseBody
	public String updateJiraComponentById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

		JSONObject json = new JSONObject();
		json.put("name", "Again component");
		json.put("leadUserName", "admin");
		json.put("description", "Description Again");
		System.out.println(json);

		byte[] credential = Base64.encodeBase64((jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

		HttpResponse<JsonNode> response = Unirest.put("https://jira-service.atlassian.net/rest/api/2/component/10000")
		  .header("Authorization","Basic " + credentialEnc)
		  .header("Accept", "application/json")
		  .header("Content-Type", "application/json")
		  .body(json)
		  .asJson();

		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	@DeleteMapping("/deleteJiraComponentById")
	@ResponseBody
	public String deleteJiraComponentById(@RequestBody JiraBean jiraBean) throws IOException, UnirestException, Exception {

        byte[] credential = Base64.encodeBase64( (jiraBean.getUsername()+":"+jiraBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.delete("https://"+jiraBean.getBaseUrl()+".atlassian.net/rest/api/2/component/10001")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
}
